package cn.huanzi.qch.baseadmin.config.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Primary
public class AuthenticationEventListener implements AuthenticationEventPublisher {
    @Override
    public void publishAuthenticationSuccess(Authentication authentication) {
        log.info(("Success event of type: "+authentication.toString()));
    }

    @Override
    public void publishAuthenticationFailure(AuthenticationException exception, Authentication authentication) {
        log.warn(("Failure event of type: "+authentication.getPrincipal().toString()+": "
                +":"+authentication.getDetails().toString()));

    }
}
