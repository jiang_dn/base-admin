package cn.huanzi.qch.baseadmin.config.security;

import cn.huanzi.qch.baseadmin.sys.sysauthority.service.SysAuthorityService;
import cn.huanzi.qch.baseadmin.sys.sysauthority.vo.SysAuthorityVo;
import cn.huanzi.qch.baseadmin.util.UUIDUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.vote.RoleVoter;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.authentication.DefaultAuthenticationEventPublisher;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private UserDetailsService defaultUserDetailsService;
    @Autowired
    private CaptchaFilterConfig captchaFilterConfig;

    @Autowired
    private UserConfig userConfig;

    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @Autowired
    private PasswordConfig passwordConfig;

    @Autowired
    private LoginFailureHandlerConfig loginFailureHandlerConfig;

    @Autowired
    private LoginSuccessHandlerConfig loginSuccessHandlerConfig;

    @Autowired
    private LogoutHandlerConfig logoutHandlerConfig;
    @Autowired
    private  LogoutSuccessHandlerConfig  logoutSuccessHandlerConfig;

    @Autowired
    private SysAuthorityService sysAuthorityService;

    @Autowired
    private MyFilterInvocationSecurityMetadataSource myFilterInvocationSecurityMetadataSource;

    @Autowired
    private DataSource dataSource;
    @Autowired
    AuthenticationEventListener authenticationEventListener;

    //无需权限访问的URL，不建议用/**/与/*.后缀同时去适配，有可以会受到CaptchaFilterConfig判断的影响
    public static final String[] MATCHERS_PERMITALL_URL = {
            "/login",
            "/get_data",
            "/logout",
            "/loginPage",
            "/favicon.ico",
            "/common/**",
            "/webjars/**",
            "/getVerifyCodeImage",
            "/error/*",
            "/test/**",
            "/user/uploadFile",
            "/openApi/*",
            "/dsl/*"
    };
    //英[ɔːˌθentɪˈkeɪʃn]

    // authorization 英[ˌɔːθəraɪˈzeɪʃn]
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

       // defaultUserDetailsService.loadUserByUsername("").

        //  配置登录监听,一种方法是实现接口类 AuthenticationEventListener

      //  auth.authenticationEventPublisher(new AuthenticationEventListener())
        // 或者注入方式
        // 或者采用authenticationEventPublisher() 中的监听事件ApplicationListener
        // 多种验证方法添加authenticationProvider()
        //ldapAuthentication LDAP 验证
        auth.authenticationEventPublisher( authenticationEventListener)

                //用户认证处理
                .userDetailsService(userConfig)
                //密码处理
                .passwordEncoder(passwordConfig);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //no-cache
        //必须先与服务器确认返回的响应是否被更改，然后才能使用该响应来满足后续对同一个网址的请求。因此，如果存在合适的验证令牌 (ETag)，no-cache 会发起往返通信来验证缓存的响应，如果资源未被更改，可以避免下载。
        //no-store
        //所有内容都不会被缓存到缓存或 Internet 临时文件中
        //must-revalidation/proxy-revalidation
        //如果缓存的内容失效，请求必须发送到服务器/代理以进行重新验证
        //max-age=xxx (xxx is numeric)
        //缓存的内容将在 xxx 秒后失效, 这个选项只在HTTP 1.1可用, 并如果和Last-Modified一起使用时, 优先级较高

        http.headers().cacheControl().and();

        //
        // X-Frame-Options：
        //他的值有三个：
        //
        //（1）DENY --- 表示该页面不允许在 frame 中展示，即便是在相同域名的页面中嵌套也不允许。
        //
        //（2）SAMEORIGIN --- 表示该页面可以在相同域名页面的 frame 中展示。
        //
        //（3）ALLOW-FROM https://example.com/ --- 表示该页面可以在指定来源的 frame 中展示。

        http.headers().frameOptions().and();
       // http.headers().contentSecurityPolicy().and();  ----https://www.bbsmax.com/A/1O5Eby44d7/ 设置XSS(跨域脚本攻击)
        http.headers().cacheControl().and();// 对页面缓存的处理
        http.headers().contentTypeOptions().and();//设置"X-Content-Type-Options: nosniff"响应标头
        http.headers().xssProtection().and();
        //http.headers().featurePolicy().and();将允许站点启用或禁用某些浏览器功能和API
        http.headers().httpPublicKeyPinning().and();
        http.headers().httpStrictTransportSecurity().and();// 必须是https
        http.headers().referrerPolicy().and();//


        //   如果开启csrf ,必须前台页面获取到X-XSRF-TOKE,后端也必须开启csrfTokenRepository,指定token 生成方式,
        //对于resful 接口,需要增加白名单,利用ignoringAntMatchers 过滤掉不需要使用csrf的
        http.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).ignoringAntMatchers(MATCHERS_PERMITALL_URL).and()
           //  关闭csrf防护  .csrf().disable()
                .headers().frameOptions().disable()
                .and();
//        http
//                // 关闭csrf防护
//                .csrf().disable()
//                .headers().frameOptions().disable()
//                .and();

        //addFilterBefore(Filter filter, Class<? extends Filter> beforeFilter)
        //在 beforeFilter 之前添加 filter
        //addFilterAfter(Filter filter, Class<? extends Filter> afterFilter)
        //在 afterFilter 之后添加 filter
        //addFilterAt(Filter filter, Class<? extends Filter> atFilter)
        //在 atFilter 相同位置添加 filter， 此 filter 不覆盖 filter

        http
                //登录处理 在指定的beforeFilter之前加入filter
                // 处理验证码
                .addFilterBefore(captchaFilterConfig, UsernamePasswordAuthenticationFilter.class)
                .formLogin()
                .loginProcessingUrl("/login")
                //未登录时默认跳转页面
                .loginPage("/loginPage")
                .failureHandler(loginFailureHandlerConfig)
                .successHandler(loginSuccessHandlerConfig)
                .permitAll()
                .and();
        http
                //登出处理
                .logout()
                .addLogoutHandler(logoutHandlerConfig)
                .logoutUrl("/logout")
                .logoutSuccessUrl("/loginPage")
               //  其中logoutSuccessUrl 和 .logoutSuccessHandler(logoutSuccessHandlerConfig) 重复

                .logoutRequestMatcher(new AntPathRequestMatcher("/logout", "GET"))
                .permitAll()
                .deleteCookies("JSESSIONID")
                .invalidateHttpSession(true)
                .clearAuthentication(true)
                .and();
        http
                //定制url访问权限，动态权限读取，参考：https://www.jianshu.com/p/0a06496e75ea
                .addFilterAfter(dynamicallyUrlInterceptor(), FilterSecurityInterceptor.class)
                .authorizeRequests()

                //无需权限访问
                .antMatchers(MATCHERS_PERMITALL_URL).permitAll()

                //其他接口需要登录后才能访问
                .anyRequest().authenticated()
                .and();

        http
                //开启记住我
                .rememberMe()
                .tokenValiditySeconds(604800)//七天免登陆
                .tokenRepository(persistentTokenRepository())
                .userDetailsService(userConfig)
                .and();
    }

    /**
     * Description:认证事件发布器
     *
     * @Author:
     * @Date: 2017/8/1 16:55
     */
    @Bean

    public AuthenticationEventPublisher authenticationEventPublisher() {
        return new DefaultAuthenticationEventPublisher();
    }

//    @Bean
//    public EntityManagerFactory entityManagerFactory() {
//        return Persistence.createEntityManagerFactory("my-persistence-unit", getJpaProperties());
//    }
//
//    private Properties getJpaProperties() {
//        Properties properties = new Properties();
//        properties.put("javax.persistence.provider", "org.hibernate.jpa.HibernatePersistenceProvider");
//        return properties;
//    }

    @Bean

    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl persistentTokenRepository = new JdbcTokenRepositoryImpl();
        persistentTokenRepository.setDataSource(dataSource);
        return persistentTokenRepository;
    }
    @Bean()
    public MyPersistentTokenBasedRememberMeServices myPersistentTokenBasedRememberMeServices() {
        MyPersistentTokenBasedRememberMeServices rememberMeServices = new MyPersistentTokenBasedRememberMeServices(UUIDUtil.getUuid(), userDetailsServiceImpl,persistentTokenRepository());
        rememberMeServices.setAlwaysRemember(true);
        return rememberMeServices;
    }
    //配置filter
    @Bean
    public DynamicallyUrlInterceptor dynamicallyUrlInterceptor(){
        //首次获取
        List<SysAuthorityVo> authorityVoList = sysAuthorityService.list(new SysAuthorityVo()).getData();
        myFilterInvocationSecurityMetadataSource.setRequestMap(authorityVoList);
        //初始化拦截器并添加数据源
        DynamicallyUrlInterceptor interceptor = new DynamicallyUrlInterceptor();
        interceptor.setSecurityMetadataSource(myFilterInvocationSecurityMetadataSource);

        //配置RoleVoter决策
        List<AccessDecisionVoter<? extends Object>> decisionVoters = new ArrayList<>();
        decisionVoters.add(new RoleVoter());

        //设置认证决策管理器
        interceptor.setAccessDecisionManager(new MyAccessDecisionManager(decisionVoters));
        return interceptor;
    }
    @Bean
    SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }
}