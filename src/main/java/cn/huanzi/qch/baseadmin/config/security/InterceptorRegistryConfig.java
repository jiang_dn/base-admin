package cn.huanzi.qch.baseadmin.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Copyright (C) 东软熙康健康科技有限公司 All Rights Reserved.FileName: cn.huanzi.qch.baseadmin.config.cors/base-admin,
 *
 * @PACKAGE cn.huanzi.qch.baseadmin.config.cors
 * @USER jiangdn
 * @DATE 2021/7/28
 * @TIME 下午4:47
 * @PROJECT base-admin
 * @Description: jiang.dn@neusoft.com
 * @Description: TODO
 */
@Configuration
public class InterceptorRegistryConfig {
    public static final String[] MATCHERS_PERMITALL_URL = {
            "/login",
            "/logout",
            "/loginPage",
            "/favicon.ico",
            "/common/**",
            "/webjars/**",
            "/getVerifyCodeImage",
            "/error/*",
            "/test/**",
            "classpath:/public/",
            "classpath:/static/",

            "/openApi/*"
    };

    @Bean
    public WebMvcConfigurer Interceptors(){

        return new WebMvcConfigurer(){
            @Override
            public void addInterceptors(InterceptorRegistry registry){

                registry.addInterceptor(new MyInterceptor()).addPathPatterns("/**").
                        excludePathPatterns(MATCHERS_PERMITALL_URL).excludePathPatterns("/static/**")
                        .excludePathPatterns("classpath:/static/")
                        .excludePathPatterns("classpath:/public/")
                        .excludePathPatterns("/public/**")

                ;

            }

        };

    };

        }



