package cn.huanzi.qch.baseadmin.config.security;

import lombok.SneakyThrows;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Copyright (C) 东软熙康健康科技有限公司 All Rights Reserved.FileName: cn.huanzi.qch.baseadmin.config.security/base-admin,
 *
 * @PACKAGE cn.huanzi.qch.baseadmin.config.security
 * @USER jiangdn
 * @DATE 2021/12/5
 * @TIME 下午4:48
 * @PROJECT base-admin
 * @Description: jiang.dn@neusoft.com
 * @Description: TODO
 */

public class MyBasicAuthenticationEntryPoint extends BasicAuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        response.addHeader("WWW-Authenticate", "Basic realm=" + getRealmName());
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        PrintWriter printWriter = new PrintWriter(response.getOutputStream());
        printWriter.write("Http Status 401: " + authException.getLocalizedMessage());
    }

    @Override
    @SneakyThrows
    public void afterPropertiesSet() {
        setRealmName("developlee");
        super.afterPropertiesSet();
    }
}
